import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import sys

df = pd.read_csv(f"/rapids/data/raw/p03/empatica_accelerometer_raw.csv")


df['date'] = pd.to_datetime(df['timestamp'],unit='ms')
df.set_index('date', inplace=True)
print(df)
df = df['double_values_0'].resample("31ms").mean()
print(df)

st='2021-05-21 12:28:27'
en='2021-05-21 12:59:12'

df = df.loc[(df.index > st) & (df.index < en)]
plt.plot(df)

plt.savefig(f'NaN.png')
sys.exit()


plt.plot(df)

esm = pd.read_csv(f"/rapids/data/raw/p03/phone_esm_raw.csv")

esm['date'] = pd.to_datetime(esm['timestamp'],unit='ms')
esm = esm[esm['date']]
esm.set_index('date', inplace=True)
print(esm)

esm = esm['esm_session'].resample("2900ms").mean()

plt.plot(esm)
plt.savefig(f'NaN.png')
