import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import os, sys

participant = "p032"

folder = f"/rapids/data/processed/features/{participant}/"
for filename in os.listdir(folder):
    if filename.startswith("phone_"):
        df = pd.read_csv(f"{folder}{filename}")
        plt.figure()
        sns.heatmap(df[[col for col in df if col.startswith('phone_')]], cbar=True)
        plt.savefig(f'{participant}_{filename}.png', bbox_inches='tight')
        plt.close()

        plt.figure()
        sns.heatmap(df[[col for col in df if col.startswith('phone_')]].isna(), cbar=True)
        plt.savefig(f'is_na_{participant}_{filename}.png', bbox_inches='tight')
        plt.close()
        
        