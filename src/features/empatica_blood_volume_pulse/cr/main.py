import pandas as pd
from sklearn.preprocessing import StandardScaler

from cr_features.helper_functions import convert_to2d, hrv_features
from cr_features.hrv import extract_hrv_features_2d_wrapper
from cr_features_helper_methods import extract_second_order_features

import sys

# pd.set_option('display.max_rows', 1000)
pd.set_option('display.max_columns', None)

def extract_bvp_features_from_intraday_data(bvp_intraday_data, features, window_length, time_segment, filter_data_by_segment):
    bvp_intraday_features = pd.DataFrame(columns=["local_segment"] + features)

    if not bvp_intraday_data.empty:
        sample_rate = 64
     
        bvp_intraday_data = filter_data_by_segment(bvp_intraday_data, time_segment)

        if not bvp_intraday_data.empty:

            bvp_intraday_features = pd.DataFrame()

            # apply methods from calculate features module
            if window_length is None:
                bvp_intraday_features = \
                    bvp_intraday_data.groupby('local_segment').apply(\
                    lambda x: 
                        extract_hrv_features_2d_wrapper(
                            convert_to2d(x['blood_volume_pulse'], x.shape[0]), 
                            sampling=sample_rate, hampel_fiter=False, median_filter=False, mod_z_score_filter=True, feature_names=features))

            else:
                bvp_intraday_features = \
                    bvp_intraday_data.groupby('local_segment').apply(\
                    lambda x: 
                        extract_hrv_features_2d_wrapper(
                            convert_to2d(x['blood_volume_pulse'], window_length*sample_rate), 
                            sampling=sample_rate, hampel_fiter=False, median_filter=False, mod_z_score_filter=True, feature_names=features)) 

            bvp_intraday_features.reset_index(inplace=True)

    return bvp_intraday_features


def cr_features(sensor_data_files, time_segment, provider, filter_data_by_segment, *args, **kwargs):
    bvp_intraday_data = pd.read_csv(sensor_data_files["sensor_data"])

    requested_intraday_features = provider["FEATURES"]
    
    calc_windows = kwargs.get('calc_windows', False)

    if provider["WINDOWS"]["COMPUTE"] and calc_windows:
        requested_window_length = provider["WINDOWS"]["WINDOW_LENGTH"]
    else:
        requested_window_length = None

    # name of the features this function can compute
    base_intraday_features_names = hrv_features
    # the subset of requested features this function can compute
    intraday_features_to_compute = list(set(requested_intraday_features) & set(base_intraday_features_names))

    # extract features from intraday data
    bvp_intraday_features = extract_bvp_features_from_intraday_data(bvp_intraday_data, intraday_features_to_compute, 
                                                                requested_window_length, time_segment, filter_data_by_segment)
                                                                
    if calc_windows:
        so_features_names = provider["WINDOWS"]["SECOND_ORDER_FEATURES"]
        bvp_second_order_features = extract_second_order_features(bvp_intraday_features, so_features_names)
        return bvp_intraday_features, bvp_second_order_features

    return bvp_intraday_features