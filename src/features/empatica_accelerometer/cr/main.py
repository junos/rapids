import pandas as pd
from scipy.stats import entropy

from cr_features.helper_functions import convert_to2d, accelerometer_features, frequency_features
from cr_features.calculate_features_old import calculateFeatures
from cr_features.calculate_features import calculate_features
from cr_features_helper_methods import extract_second_order_features

import sys

def extract_acc_features_from_intraday_data(acc_intraday_data, features, window_length, time_segment, filter_data_by_segment):
    acc_intraday_features = pd.DataFrame(columns=["local_segment"] + features)

    if not acc_intraday_data.empty:   
        sample_rate = 32
     
        acc_intraday_data = filter_data_by_segment(acc_intraday_data, time_segment)

        if not acc_intraday_data.empty:

            acc_intraday_features = pd.DataFrame()

            # apply methods from calculate features module
            if window_length is None:
                acc_intraday_features = \
                    acc_intraday_data.groupby('local_segment').apply(lambda x: calculate_features( \
                    convert_to2d(x['double_values_0'], x.shape[0]), \
                    convert_to2d(x['double_values_1'], x.shape[0]), \
                    convert_to2d(x['double_values_2'], x.shape[0]), \
                    fs=sample_rate, feature_names=features, show_progress=False)) 
            else:
                acc_intraday_features = \
                    acc_intraday_data.groupby('local_segment').apply(lambda x: calculate_features( \
                    convert_to2d(x['double_values_0'], window_length*sample_rate), \
                    convert_to2d(x['double_values_1'], window_length*sample_rate), \
                    convert_to2d(x['double_values_2'], window_length*sample_rate), \
                    fs=sample_rate, feature_names=features, show_progress=False)) 

            acc_intraday_features.reset_index(inplace=True)

    return acc_intraday_features



def cr_features(sensor_data_files, time_segment, provider, filter_data_by_segment, *args, **kwargs):
    
    data_types = {'local_timezone': 'str', 'device_id': 'str', 'timestamp': 'int64', 'double_values_0': 'float64',
                    'double_values_1': 'float64', 'double_values_2': 'float64', 'local_date_time': 'str', 'local_date': "str",
                    'local_time': "str", 'local_hour': "str", 'local_minute': "str", 'assigned_segments': "str"}
    acc_intraday_data = pd.read_csv(sensor_data_files["sensor_data"], dtype=data_types)    

    requested_intraday_features = provider["FEATURES"]
    
    calc_windows = kwargs.get('calc_windows', False)

    if provider["WINDOWS"]["COMPUTE"] and calc_windows:
        requested_window_length = provider["WINDOWS"]["WINDOW_LENGTH"]
    else:
        requested_window_length = None

    # name of the features this function can compute
    base_intraday_features_names = accelerometer_features + frequency_features
    # the subset of requested features this function can compute
    intraday_features_to_compute = list(set(requested_intraday_features) & set(base_intraday_features_names))

    # extract features from intraday data
    acc_intraday_features = extract_acc_features_from_intraday_data(acc_intraday_data, intraday_features_to_compute, 
                                                                requested_window_length, time_segment, filter_data_by_segment)

    if calc_windows:
        so_features_names = provider["WINDOWS"]["SECOND_ORDER_FEATURES"]
        acc_second_order_features = extract_second_order_features(acc_intraday_features, so_features_names)
        return acc_intraday_features, acc_second_order_features

    return acc_intraday_features