import pandas as pd
from scipy.stats import entropy

from cr_features.helper_functions import convert_to2d, generic_features
from cr_features.calculate_features_old import calculateFeatures
from cr_features.calculate_features import calculate_features
from cr_features_helper_methods import extract_second_order_features

import sys

def extract_temp_features_from_intraday_data(temperature_intraday_data, features, window_length, time_segment, filter_data_by_segment):
    temperature_intraday_features = pd.DataFrame(columns=["local_segment"] + features)

    if not temperature_intraday_data.empty:
        sample_rate = 4

        temperature_intraday_data = filter_data_by_segment(temperature_intraday_data, time_segment)

        if not temperature_intraday_data.empty:

            temperature_intraday_features = pd.DataFrame()

            # apply methods from calculate features module
            if window_length is None:
                temperature_intraday_features = \
                    temperature_intraday_data.groupby('local_segment').apply(\
                    lambda x: calculate_features(convert_to2d(x['temperature'], x.shape[0]), fs=sample_rate, feature_names=features, show_progress=False))
            else:
                temperature_intraday_features = \
                    temperature_intraday_data.groupby('local_segment').apply(\
                    lambda x: calculate_features(convert_to2d(x['temperature'], window_length*sample_rate), fs=sample_rate, feature_names=features, show_progress=False))


            temperature_intraday_features.reset_index(inplace=True)

    return temperature_intraday_features


def cr_features(sensor_data_files, time_segment, provider, filter_data_by_segment, *args, **kwargs):
    data_types = {'local_timezone': 'str', 'device_id': 'str', 'timestamp': 'int64', 'temperature': 'float64', 'local_date_time': 'str', 
                'local_date': "str", 'local_time': "str", 'local_hour': "str", 'local_minute': "str", 'assigned_segments': "str"}

    temperature_intraday_data = pd.read_csv(sensor_data_files["sensor_data"], dtype=data_types)

    requested_intraday_features = provider["FEATURES"]

    calc_windows = kwargs.get('calc_windows', False)

    if provider["WINDOWS"]["COMPUTE"] and calc_windows:
        requested_window_length = provider["WINDOWS"]["WINDOW_LENGTH"]
    else:
        requested_window_length = None
    
    # name of the features this function can compute
    base_intraday_features_names = generic_features
    # the subset of requested features this function can compute
    intraday_features_to_compute = list(set(requested_intraday_features) & set(base_intraday_features_names))

    # extract features from intraday data
    temperature_intraday_features = extract_temp_features_from_intraday_data(temperature_intraday_data, intraday_features_to_compute, 
                                                                        requested_window_length, time_segment, filter_data_by_segment)

    if calc_windows:
        so_features_names = provider["WINDOWS"]["SECOND_ORDER_FEATURES"]
        temperature_second_order_features = extract_second_order_features(temperature_intraday_features, so_features_names)
        return temperature_intraday_features, temperature_second_order_features

    return temperature_intraday_features