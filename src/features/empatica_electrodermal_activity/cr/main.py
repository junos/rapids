import pandas as pd
import numpy as np
from scipy.stats import entropy

from cr_features.helper_functions import convert_to2d, gsr_features
from cr_features.calculate_features import calculate_features
from cr_features.gsr import extractGsrFeatures2D
from cr_features_helper_methods import extract_second_order_features

import sys

#pd.set_option('display.max_columns', None)
#pd.set_option('display.max_rows', None)
#np.seterr(invalid='ignore')


def extract_eda_features_from_intraday_data(eda_intraday_data, features, window_length, time_segment, filter_data_by_segment):
    eda_intraday_features = pd.DataFrame(columns=["local_segment"] + features)

    if not eda_intraday_data.empty:   
        sample_rate = 4  
     
        eda_intraday_data = filter_data_by_segment(eda_intraday_data, time_segment)

        if not eda_intraday_data.empty: 

            eda_intraday_features = pd.DataFrame()

            # apply methods from calculate features module 
            if window_length is None:
                eda_intraday_features = \
                    eda_intraday_data.groupby('local_segment').apply(\
                    lambda x: extractGsrFeatures2D(convert_to2d(x['electrodermal_activity'], x.shape[0]), sampleRate=sample_rate, featureNames=features,
                    threshold=.01, offset=1, riseTime=5, decayTime=15)) 
            else:
                eda_intraday_features = \
                    eda_intraday_data.groupby('local_segment').apply(\
                    lambda x: extractGsrFeatures2D(convert_to2d(x['electrodermal_activity'], window_length*sample_rate), sampleRate=sample_rate, featureNames=features,
                    threshold=.01, offset=1, riseTime=5, decayTime=15)) 

            eda_intraday_features.reset_index(inplace=True)

    return eda_intraday_features


def cr_features(sensor_data_files, time_segment, provider, filter_data_by_segment, *args, **kwargs):

    data_types = {'local_timezone': 'str', 'device_id': 'str', 'timestamp': 'int64', 'electrodermal_activity': 'float64', 'local_date_time': 'str', 
                  'local_date': "str", 'local_time': "str", 'local_hour': "str", 'local_minute': "str", 'assigned_segments': "str"}

    eda_intraday_data = pd.read_csv(sensor_data_files["sensor_data"], dtype=data_types)

    requested_intraday_features = provider["FEATURES"]
    
    calc_windows = kwargs.get('calc_windows', False)

    if provider["WINDOWS"]["COMPUTE"] and calc_windows:
        requested_window_length = provider["WINDOWS"]["WINDOW_LENGTH"]
    else:
        requested_window_length = None

    # name of the features this function can compute
    base_intraday_features_names = gsr_features
    # the subset of requested features this function can compute
    intraday_features_to_compute = list(set(requested_intraday_features) & set(base_intraday_features_names))

    # extract features from intraday data
    eda_intraday_features = extract_eda_features_from_intraday_data(eda_intraday_data, intraday_features_to_compute, 
                                                                requested_window_length, time_segment, filter_data_by_segment)

    if calc_windows:
        if provider["WINDOWS"]["IMPUTE_NANS"]:
            eda_intraday_features[eda_intraday_features["numPeaks"] == 0] = \
                eda_intraday_features[eda_intraday_features["numPeaks"] == 0].fillna(0)
            pd.set_option('display.max_columns', None)
            
        so_features_names = provider["WINDOWS"]["SECOND_ORDER_FEATURES"]
        eda_second_order_features = extract_second_order_features(eda_intraday_features, so_features_names)
    
        return eda_intraday_features, eda_second_order_features

    return eda_intraday_features