import pandas as pd

from helper import retain_target_column

cleaned_sensor_features = pd.read_csv(snakemake.input["cleaned_sensor_features"])
target_variable_name = snakemake.params["target_variable"]

model_input = retain_target_column(cleaned_sensor_features, target_variable_name)

if model_input is None:
    pd.DataFrame().to_csv(snakemake.output[0])
else:
    model_input.to_csv(snakemake.output[0], index=False)
