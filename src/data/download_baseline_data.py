import pandas as pd
import yaml

filename = snakemake.input["data"]
baseline = pd.read_csv(filename)

with open(snakemake.input["participant_file"], "r") as file:
    participant = yaml.safe_load(file)

username = participant["PHONE"]["LABEL"]

baseline[baseline["username"] == username].to_csv(snakemake.output[0],
                                                  index=False,
                                                  encoding="utf-8",)
